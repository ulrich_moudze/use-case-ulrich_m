package wf.ulrich.devoxx;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import wf.ulrich.devoxx.domain.model.Commande;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class IntegrationUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String readFromFileToString(String filePath) throws IOException {
        File resource = new ClassPathResource(filePath).getFile();
        byte[] byteArray = Files.readAllBytes(resource.toPath());
        return new String(byteArray);
    }

    public static Commande deserializeProduct(String json) throws JsonProcessingException {
        return objectMapper.readValue(json, Commande.class);
    }

}
