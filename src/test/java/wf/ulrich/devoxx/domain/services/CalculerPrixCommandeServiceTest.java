package wf.ulrich.devoxx.domain.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import wf.ulrich.devoxx.IntegrationUtils;
import wf.ulrich.devoxx.common.exception.GlobalException;
import wf.ulrich.devoxx.common.exception.ParameterNotFoundException;
import wf.ulrich.devoxx.domain.model.Commande;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class CalculerPrixCommandeServiceTest {


    @InjectMocks
    private CalculerPrixCommandeService calculerPrixCommandeService;

    @Autowired
    private IntegrationUtils integrationUtils;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void evaluerCas1SuccessTest() throws IOException, ParameterNotFoundException, GlobalException {
        //Given
        String commandeJson = IntegrationUtils.readFromFileToString("json/cas1.json");
        String resultatAttenduJson = IntegrationUtils.readFromFileToString("json/cas1_response.json");
        Commande commande = IntegrationUtils.deserializeProduct(commandeJson);
        Commande ResultatAttendu = IntegrationUtils.deserializeProduct(resultatAttenduJson);

        //When
        Commande commandeResponse = calculerPrixCommandeService.calculerPrixCommande(commande);

        //Then
        assertEquals(ResultatAttendu, commandeResponse);

    }

    @Test
    public void evaluerCas2SuccessTest() throws IOException, ParameterNotFoundException, GlobalException {
        //Given
        String commandeJson = IntegrationUtils.readFromFileToString("json/cas2.json");
        String resultatAttenduJson = IntegrationUtils.readFromFileToString("json/cas2_response.json");
        Commande commande = IntegrationUtils.deserializeProduct(commandeJson);
        Commande ResultatAttendu = IntegrationUtils.deserializeProduct(resultatAttenduJson);

        //When
        Commande commandeResponse = calculerPrixCommandeService.calculerPrixCommande(commande);

        //Then
        assertEquals(ResultatAttendu, commandeResponse);

    }

}