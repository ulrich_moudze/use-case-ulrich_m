package wf.ulrich.devoxx.application.rest;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wf.ulrich.devoxx.application.rest.response.PrixCommandeResponse;
import wf.ulrich.devoxx.common.exception.GlobalException;
import wf.ulrich.devoxx.common.exception.ParameterNotFoundException;
import wf.ulrich.devoxx.domain.model.Commande;
import wf.ulrich.devoxx.domain.ports.inbound.CalculerPrixCommandeUseCase;

@RestController
@RequestMapping("/api/commande")
@NoArgsConstructor
public class CommandeController {

    @Autowired
    private CalculerPrixCommandeUseCase calculerPrixCommandeUseCase;

    public CommandeController(CalculerPrixCommandeUseCase calculerPrixCommandeUseCase){
        this.calculerPrixCommandeUseCase = calculerPrixCommandeUseCase;
    }


    @PostMapping
    public ResponseEntity<PrixCommandeResponse> getPrix(@RequestBody Commande commande) throws GlobalException, ParameterNotFoundException {
        var response = calculerPrixCommandeUseCase.calculerPrixCommande(commande);
        return ResponseEntity.ok().body(PrixCommandeResponse.builder().description(response).build());
    }

}
