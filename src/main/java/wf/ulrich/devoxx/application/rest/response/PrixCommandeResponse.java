package wf.ulrich.devoxx.application.rest.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import wf.ulrich.devoxx.domain.model.Commande;

@Getter
@Setter
@Builder
public class PrixCommandeResponse {

    private Commande description;
}
