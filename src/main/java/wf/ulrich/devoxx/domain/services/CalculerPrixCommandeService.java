package wf.ulrich.devoxx.domain.services;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import wf.ulrich.devoxx.common.exception.GlobalException;
import wf.ulrich.devoxx.common.exception.ParameterNotFoundException;
import wf.ulrich.devoxx.domain.model.Commande;
import wf.ulrich.devoxx.domain.model.LigneCommande;
import wf.ulrich.devoxx.domain.ports.inbound.CalculerPrixCommandeUseCase;

import java.util.ArrayList;
import java.util.List;

import static wf.ulrich.devoxx.common.constants.CommandeConstants.TAUX_DROIT_IMPORTATION;
import static wf.ulrich.devoxx.common.constants.CommandeConstants.TAUX_TAXES_VENTE_DE_BASE;
import static wf.ulrich.devoxx.domain.enumeration.TypeArticleEnum.AUTRE;

@NoArgsConstructor
@Service
@Data
public class CalculerPrixCommandeService implements CalculerPrixCommandeUseCase {



    @Override
    public Commande calculerPrixCommande(Commande commande) throws GlobalException, ParameterNotFoundException {
        var prixTotal = 0F;
        List<LigneCommande> nouvelleLigneCommandeList = new ArrayList<>();
        List<LigneCommande> ligneCommandeList = commande.getLigneCommandeList();
        for(LigneCommande ligneCommande : ligneCommandeList){
            LigneCommande nouvelleLigneCommande =  calculPrixParLigneCommande(ligneCommande);
            nouvelleLigneCommandeList.add(nouvelleLigneCommande);
            prixTotal = prixTotal + nouvelleLigneCommande.getPrixTotalLigneCommande();
        }
        commande.setPrixTotal(prixTotal);
        commande.setLigneCommandeList(nouvelleLigneCommandeList);
        //commande.setTaxeDeVente();

        return commande;
    }

    private LigneCommande calculPrixParLigneCommande (LigneCommande ligneCommande){

        var prixTotalParLigneCommande = 0F;
        float montantTotalTaxeDeVente = getMontantTotalTaxeDeVente(ligneCommande);
        float montantTotalDroitImportation = getMontantTotalDroitImportation(ligneCommande);
        prixTotalParLigneCommande = ligneCommande.getQuantite() * ligneCommande.getArticle().getPrix() + montantTotalTaxeDeVente + montantTotalDroitImportation;
        ligneCommande.setPrixTotalLigneCommande(prixTotalParLigneCommande);
        return ligneCommande;

    }

    private static float getMontantTotalTaxeDeVente(LigneCommande ligneCommande) {
        var montantTotalTaxeDeVente = 0F;
        if(AUTRE.name().equals(ligneCommande.getArticle().getTypeArticle())){
            montantTotalTaxeDeVente = ligneCommande.getQuantite() * ligneCommande.getArticle().getPrix() * TAUX_TAXES_VENTE_DE_BASE;
        }
        return montantTotalTaxeDeVente;
    }

    private static float getMontantTotalDroitImportation(LigneCommande ligneCommande) {
        var montantTotalDroitImportation = 0F;
        if(ligneCommande.getArticle().isImporte()){
            montantTotalDroitImportation = ligneCommande.getQuantite() * ligneCommande.getArticle().getPrix() * TAUX_DROIT_IMPORTATION;

        }
        return montantTotalDroitImportation;
    }
}
