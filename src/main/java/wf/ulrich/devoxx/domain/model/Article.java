package wf.ulrich.devoxx.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties
public class Article {

    private Long id;

    private String titre;

    private Float prix;

    private String typeArticle;

    private boolean importe;

}
