package wf.ulrich.devoxx.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties
@AllArgsConstructor
@NoArgsConstructor
public class Commande implements Serializable {

    private List<LigneCommande> ligneCommandeList;
    private Float prixTotal;
    private Float taxeDeVente;

}
