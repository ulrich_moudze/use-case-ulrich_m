package wf.ulrich.devoxx.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties
public class LigneCommande {

    private float prixTotalLigneCommande;

    private int quantite;

    private Article article;
}
