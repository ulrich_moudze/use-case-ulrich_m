package wf.ulrich.devoxx.domain.ports.inbound;

import wf.ulrich.devoxx.common.exception.GlobalException;
import wf.ulrich.devoxx.common.exception.ParameterNotFoundException;
import wf.ulrich.devoxx.domain.model.Commande;


public interface CalculerPrixCommandeUseCase {

    Commande calculerPrixCommande(Commande commande) throws GlobalException, ParameterNotFoundException;
}
