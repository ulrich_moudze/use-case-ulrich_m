package wf.ulrich.devoxx.common.constants;

public class CommandeConstants {

    public static final float TAUX_TAXES_VENTE_DE_BASE = 0.1F;

    public static final float TAUX_DROIT_IMPORTATION = 0.05F;

}
